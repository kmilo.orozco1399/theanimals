﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class D_Login
    {
        public static DataTable ValidacionDBIngreso(string usuario, string contrasena)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("consulta_ingreso", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable general = new DataTable();
            consulta.SelectCommand.Parameters.Add("@usuario", SqlDbType.VarChar, 20).Value = usuario;
            consulta.SelectCommand.Parameters.Add("@contrasena", SqlDbType.VarChar, 20).Value = contrasena;
            consulta.Fill(general);
            miconexion.Close();
            return general;
        }
    }
}
