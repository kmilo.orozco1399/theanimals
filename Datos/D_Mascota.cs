﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
     public class D_Mascota
    {

        public static DataTable ListaMascotas(string cadenaBusqueda)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("ListaMascotas", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            consulta.SelectCommand.Parameters.Add("@cadenaBusqueda", SqlDbType.VarChar, 50).Value = cadenaBusqueda;
            DataTable ListaMascotas = new DataTable();
            consulta.Fill(ListaMascotas);
            miconexion.Close();
            return ListaMascotas;
        }




        public static int CrearMascotaDB(string NombrePropietario, int TelefonoPropietario, string NombreMascota, string RazaMascota, string EdadMascota, string GeneroMascota, string TipoMascota)
        {

            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand RegMascota = new SqlCommand("crear_mascota", miconexion);
            RegMascota.CommandType = CommandType.StoredProcedure;

            try
            {


                int user = 1;
                RegMascota.Parameters.Add("@NombrePropietario", SqlDbType.VarChar, 20).Value = NombrePropietario;
                RegMascota.Parameters.Add("@TelefonoPropietario", SqlDbType.Int).Value = TelefonoPropietario;
                RegMascota.Parameters.Add("@NombreMascota", SqlDbType.VarChar, 20).Value = NombreMascota;
                RegMascota.Parameters.Add("@RazaMascota", SqlDbType.VarChar, 20).Value = RazaMascota;
                RegMascota.Parameters.Add("@EdadMascota", SqlDbType.VarChar, 20).Value = EdadMascota;
                RegMascota.Parameters.Add("@GeneroMascota", SqlDbType.VarChar, 20).Value = GeneroMascota;
                RegMascota.Parameters.Add("@TipoMascota", SqlDbType.VarChar, 20).Value = TipoMascota;
                RegMascota.Parameters.Add("@user", SqlDbType.Int).Value = user;


                RegMascota.ExecuteNonQuery();
                return 1;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

                miconexion.Close();
            }



        }





        public static int EditarMascotaDB(int IdMascota, string NombPropE, int TelPropE, string NomMascotaE, string RazaMascotaE, string EdadMascotaE, string GeneroMascotaE, string TipoMascotaE)
        {

            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand EditarMascota = new SqlCommand("EditarMascota", miconexion);
            EditarMascota.CommandType = CommandType.StoredProcedure;

            try
            {
                EditarMascota.Parameters.Add("@IdMascota", SqlDbType.Int).Value = IdMascota;
                EditarMascota.Parameters.Add("@NombrePropietario", SqlDbType.VarChar, 20).Value = NombPropE;
                EditarMascota.Parameters.Add("@TelefonoPropietario", SqlDbType.Int).Value = TelPropE;
                EditarMascota.Parameters.Add("@NombreMascota", SqlDbType.VarChar, 20).Value = NomMascotaE;
                EditarMascota.Parameters.Add("@RazaMascota", SqlDbType.VarChar, 20).Value = RazaMascotaE;
                EditarMascota.Parameters.Add("@EdadMascota", SqlDbType.VarChar, 20).Value = EdadMascotaE;
                EditarMascota.Parameters.Add("@GeneroMascota", SqlDbType.VarChar, 20).Value = GeneroMascotaE;
                EditarMascota.Parameters.Add("@TipoMascota", SqlDbType.VarChar, 20).Value = TipoMascotaE;



                EditarMascota.ExecuteNonQuery();
                return 1;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

                miconexion.Close();
            }



        }















        public static int EliminarMascotaDB(int IdMascota)
        {

            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand EliminarMascota = new SqlCommand("eliminarMascota", miconexion);
            EliminarMascota.CommandType = CommandType.StoredProcedure;

            try
            {
                EliminarMascota.Parameters.Add("@idMascota", SqlDbType.Int).Value= IdMascota;
                EliminarMascota.ExecuteNonQuery();
                return 1;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

                miconexion.Close();
            }



        }




    }
}
