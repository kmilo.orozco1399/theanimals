﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class D_Agendamiento
    {
        public static DataTable Consulta_General()
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("ConsultaGeneralMascotasServicio", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable general = new DataTable();
            consulta.Fill(general);
            miconexion.Close();
            return general;
        }

        public static DataTable Consulta_ListaMascotas()
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("ConsultaListaMascotas", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable general = new DataTable();
            consulta.Fill(general);
            miconexion.Close();
            return general;
        }

        public static DataTable Consulta_ListaServicios()
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("ConsultaListaServicios", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable general = new DataTable();
            consulta.Fill(general);
            miconexion.Close();
            return general;
        }

        public static int Registro_MascotasServicios(int mascotas, int servicios,int usuarios, DateTime fecha)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand insertar = new SqlCommand("RegistroMascotasServicios", miconexion);
            insertar.CommandType = CommandType.StoredProcedure;
            insertar.Parameters.Add("@IdMascota", SqlDbType.Int).Value = mascotas;
            insertar.Parameters.Add("@IdServicio", SqlDbType.Int).Value = servicios;
            insertar.Parameters.Add("@IdUsuarios", SqlDbType.Int).Value = usuarios;
            insertar.Parameters.Add("@FechaServicio", SqlDbType.Date).Value = fecha;
            insertar.ExecuteNonQuery();
            miconexion.Close();
            return 0;
        }

        public static DataTable Consulta_Especifica(string busqueda)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("ConsultaEspecificaMascotasServicio", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable general = new DataTable();
            consulta.SelectCommand.Parameters.Add("@NombrePropietario", SqlDbType.VarChar, 20).Value = busqueda;
            consulta.SelectCommand.Parameters.Add("@NombreMascota", SqlDbType.VarChar, 20).Value = busqueda;
            consulta.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar, 20).Value = busqueda;
            consulta.Fill(general);
            miconexion.Close();
            return general;
        }
    }
}
