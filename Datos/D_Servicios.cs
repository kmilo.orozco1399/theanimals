﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Datos
{
    public class D_Servicios
    {
        public static int RegistroDBServicios(string Nombre, string duracion)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand insertar = new SqlCommand("registro_servicios", miconexion);
            insertar.CommandType = CommandType.StoredProcedure;
            insertar.Parameters.Add("@Nombre", SqlDbType.VarChar, 20).Value = Nombre;
            insertar.Parameters.Add("@duracion", SqlDbType.VarChar, 20).Value = duracion;
            insertar.Parameters.Add("@IdUsuarios", SqlDbType.Int).Value = 1;
            insertar.ExecuteNonQuery();
            miconexion.Close();
            return 0;

        }

        public static int ActualizarDBServicios(string Nombre, string duracion, string Id)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand insertar = new SqlCommand("actualizar_servicio", miconexion);
            insertar.CommandType = CommandType.StoredProcedure;
            insertar.Parameters.Add("@Nombre", SqlDbType.VarChar, 20).Value = Nombre;
            insertar.Parameters.Add("@duracion", SqlDbType.VarChar, 20).Value = duracion;
            insertar.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
            insertar.ExecuteNonQuery();
            miconexion.Close();
            return 0;

        }

        public static int EliminarDBServicios(string Id)
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlCommand insertar = new SqlCommand("eliminar_servicio", miconexion);
            insertar.CommandType = CommandType.StoredProcedure;
            insertar.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
            insertar.ExecuteNonQuery();
            miconexion.Close();
            return 0;

        }

        public static DataTable Consulta_General()
        {
            SqlConnection miconexion = new SqlConnection(Conexion.cadena_conexion());
            miconexion.Open();
            SqlDataAdapter consulta = new SqlDataAdapter("ConsultaGeneralServicios", miconexion);
            consulta.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable general = new DataTable();
            consulta.Fill(general);
            miconexion.Close();
            return general;
        }
    }
}
