create database theAnimals
go

use theAnimals
go

create table usuarios
(
Id int primary key identity (1,1),
Nombre varchar(20),
Apellido varchar(20),
usuario varchar(20),
contrasena varchar(20),
correo varchar(50),
telefono bigint,
cargo varchar(20),
IdUsuarios int foreign key references usuarios (Id)
)
go

create table mascotas
(
Id int primary key identity (1,1),
NombrePropietario varchar(20),
Telefonopropietario bigint,
NombreMascota varchar(20),
RazaMascota varchar(20),
EdadMascota varchar(20),
GeneroMascota varchar(20),
TipoMascota varchar(20),
IdUsuarios int foreign key references usuarios (Id)
)
go

create table servicios
(
Id int primary key identity (1,1),
Nombre varchar(20),
duracion varchar(20),
IdUsuarios int foreign key references usuarios (Id)
)
go

create table detalleMascotasServicio
(
Id int primary key identity (1,1),
IdMascota int foreign key references mascotas(Id),
IdServicio int foreign key references servicios(Id),
IdUsuarios int foreign key references usuarios (Id),
FechaServicio date
)
go

----------------------------------Tablas----------------------------------------------------
 
create proc consulta_ingreso
@usuario varchar(20),
@contrasena varchar(20)
as
select Id,usuario,contrasena from usuarios where usuario = @usuario and contrasena = @contrasena
go

create proc ConsultaGeneralMascotasServicio
as
select mascotas.NombrePropietario as Propietario, mascotas.NombreMascota as Mascota, mascotas.GeneroMascota as Genero, servicios.Nombre as Servicio, detalleMascotasServicio.FechaServicio as Fecha, servicios.duracion as Duracion
from detalleMascotasServicio inner join mascotas on detalleMascotasServicio.IdMascota = mascotas.Id inner join servicios on detalleMascotasServicio.IdServicio = servicios.Id 
go

create proc ConsultaEspecificaMascotasServicio
@NombrePropietario varchar(20),
@NombreMascota varchar(20),
@Nombre varchar(20)
as
select mascotas.NombrePropietario as Propietario, mascotas.NombreMascota as Mascota, mascotas.GeneroMascota as Genero, servicios.Nombre as Servicio, detalleMascotasServicio.FechaServicio as Fecha, servicios.duracion as Duracion
from detalleMascotasServicio inner join mascotas on detalleMascotasServicio.IdMascota = mascotas.Id inner join servicios on detalleMascotasServicio.IdServicio = servicios.Id 
where mascotas.NombrePropietario like @NombrePropietario + '%' or mascotas.NombreMascota like @NombreMascota + '%' or servicios.Nombre like @Nombre + '%'
go

create proc ConsultaListaMascotas
as
select NombreMascota as Mascota, Id as IdMascota from mascotas
go

create proc ConsultaListaServicios
as
select Nombre as Servicios, Id as IdServicios from servicios
go

create proc RegistroMascotasServicios
@IdMascota int,
@IdServicio int,
@IdUsuarios int,
@FechaServicio date
as
insert into detalleMascotasServicio values(@IdMascota,@IdServicio,@IdUsuarios,@FechaServicio)
go

--------------------------------Procedimientos Agendamiento------------------------------------------------------
create proc crear_mascota
@NombrePropietario varchar(20),
@TelefonoPropietario int,
@NombreMascota varchar(20),
@RazaMascota varchar(20),
@EdadMascota varchar(20),
@GeneroMascota varchar(20),
@TipoMascota varchar(20),
@user int
as
begin
INSERT INTO mascotas
VALUES(@NombrePropietario,
@TelefonoPropietario,
@NombreMascota,
@RazaMascota,
@EdadMascota,
@GeneroMascota,
@TipoMascota,
@user)
end
go

create proc ListaMascotas
@cadenaBusqueda NVARCHAR(50)
as
begin
if @cadenaBusqueda = ''
begin
select * from mascotas
end
else
begin
select * from mascotas
where
mascotas.NombreMascota like CONCAT('%',@cadenaBusqueda, '%')
OR mascotas.NombrePropietario like CONCAT('%',@cadenaBusqueda, '%')
end
end
go

create proc EditarMascota
@IdMascota int,
@NombrePropietario varchar(20),
@TelefonoPropietario int,
@NombreMascota varchar(20),
@RazaMascota varchar(20),
@EdadMascota varchar(20),
@GeneroMascota varchar(20),
@TipoMascota varchar(20)
as
BEGIN
UPDATE mascotas
SET
NombrePropietario = @NombrePropietario,
Telefonopropietario = @TelefonoPropietario,
NombreMascota = @NombreMascota,
RazaMascota = @RazaMascota,
EdadMascota = @EdadMascota,
GeneroMascota = @GeneroMascota,
TipoMascota = @TipoMascota
WHERE
Id = @IdMascota
END
go

create proc eliminarMascota
@idMascota int
as
begin
DELETE mascotas WHERE Id=@idMascota
end
go
--------------------------------Procedimientos Mascotas----------------------------------------------------------
create proc registro_servicios
@Nombre varchar(20),
@duracion varchar(20),
@IdUsuarios int
as 
insert into servicios values (@Nombre, @duracion,@IdUsuarios)
go

create proc actualizar_servicio
@Nombre varchar(20),
@duracion varchar(20),
@Id int
as
update servicios
set
Nombre=@Nombre,
duracion=@duracion
where Id = @Id 
go

create proc eliminar_servicio
@Id int
as
delete from servicios
where Id =@Id
go

create proc ConsultaGeneralServicios
as
select Id,Nombre,duracion from servicios
go
--------------------------------Procedimientos Servicios---------------------------------------------------------

--------------------------------Procedimientos Usuarios---------------------------------------------------------
insert into usuarios values('Juan','perez','Admin','Admin123','Admin@gmail.com',13456789,'Admin',1)
go

insert into mascotas values('Marcos',56325741,'Pepe','Criollo','2 a�os','Macho','Perro',1)
go
insert into mascotas values('Alex',36589239,'Lucas','Beagle','1 a�os','Macho','Gato',1)
go
insert into mascotas values('Andres',896358742,'Cloe','Cocker','6 meses','Hembra','Perra',1)
go
insert into mascotas values('Sandra',69874569,'Nala','Labrador','2 a�os','Hembra','Gata',1)
go

insert into servicios values('Peluqueria','20 minutos',1)
go
insert into servicios values('Ba�o','30 minutos',1)
go
insert into servicios values('Consulta general','1 hora',1)
go
insert into servicios values('Esterilizaci�n','1 hora',1)
go

insert into detalleMascotasServicio values(1,1,1,'20211024')
go
insert into detalleMascotasServicio values(2,4,1,'20211020')
go
insert into detalleMascotasServicio values(3,2,1,'20211024')
go
insert into detalleMascotasServicio values(4,3,1,'20211024')
go

------------------------------Inserciones---------------------------------------------

select * from usuarios
select * from mascotas
select * from servicios
select * from detalleMascotasServicio
