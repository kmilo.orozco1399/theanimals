﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;


namespace VeterinariaMax
{
    public partial class FrmServicios : Form
    {
        public FrmServicios()
        {
            InitializeComponent();
        }

        private void BtnRegistrar_Click(object sender, EventArgs e)

        {
            try
            {

                if (TxtRegistroServicio.Text == "" && TxtDuracionServicio.Text == "")
                {
                    MessageBox.Show("Error, No se admiten campos vacios", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (BtnRegistrar.Text == "Registrar")
                    {
                        string Nombre = TxtRegistroServicio.Text;
                        string duracion = TxtDuracionServicio.Text;
                        N_Servicios Registro = new N_Servicios();
                        int respuesta = Registro.RegistrarServicios(Nombre, duracion);




                        if (respuesta == 0)
                        {
                            MessageBox.Show("Registro los datos correctamente", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ConsultaGeneral();
                        }
                    }
                    else
                    {
                        string Nombre = TxtRegistroServicio.Text;
                        string duracion = TxtDuracionServicio.Text;
                        string Id = label3.Text;
                        N_Servicios Actualizar = new N_Servicios();
                        int respuesta = Actualizar.ActualizarServicios(Nombre, duracion, Id);
                        if (respuesta == 0)
                        {
                            MessageBox.Show("Actualizo los datos correctamente", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ConsultaGeneral();
                        }
                    }

                }

            }
            catch
            {
                MessageBox.Show("Error, revisar conexion a la base de datos", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void BtnMenu_Click(object sender, EventArgs e)
        {
            Menu retorno = new Menu();
            retorno.Show();
            this.Hide();
        }

        private void FrmServicios_Load(object sender, EventArgs e)
        {
            ConsultaGeneral();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string Id = label3.Text;
                N_Servicios Eliminar = new N_Servicios();
                int respuesta = Eliminar.EliminarServicios(Id);
                if (respuesta == 0)
                {
                    MessageBox.Show("Elimino los datos correctamente", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ConsultaGeneral();
                }
            }
            catch
            {
                MessageBox.Show("De doble click al registro a eliminar", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void ConsultaGeneral()
        {
            N_Servicios servicios = new N_Servicios();
            DataTable tabla = new DataTable();
            tabla = servicios.ConsultaGeneral();
            dataGridView1.DataSource = tabla;
        }

        private void dataGridView1_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            label3.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            TxtRegistroServicio.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            TxtDuracionServicio.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();

            BtnRegistrar.Text = "Actualizar";
        }
    }
}

