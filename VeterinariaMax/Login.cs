﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace VeterinariaMax
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            try
            
            {
                string Usuario = txtUsu.Text;
                string contrasena = txtPass.Text;
                N_Login Login = new N_Login();
                DataTable tabla = new DataTable();
                tabla = Login.ValidacionIngreso(Usuario, contrasena);
                dataGridLogin.DataSource = tabla;
                if (dataGridLogin.Rows.Count == 0)
                {
                    MessageBox.Show("usuario o contraseña estan incorrectos verificar", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.ActiveControl = txtUsu;
                    txtUsu.Focus();
                }
                else
                {
                    Menu menu = new Menu();
                    menu.Show();
                    this.Hide();
                }
            }
            catch
            {
                MessageBox.Show("Error, revisar conexion a la base de datos", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Login_Load_1(object sender, EventArgs e)
        {
            this.ActiveControl = txtUsu;
            txtUsu.Focus();
        }
    }
}
