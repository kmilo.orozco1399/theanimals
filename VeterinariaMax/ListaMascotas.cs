﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace VeterinariaMax
{
    public partial class ListaMascotas : Form
    {
        string cadenaBusqueda { get; set; }
        public ListaMascotas(string cadenaBusqueda = "")
        {
            this.cadenaBusqueda = cadenaBusqueda;
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
            string cadenaBusqueda = txtCadenBusqueda.Text;
            ListaMascotas FormLM = new ListaMascotas(cadenaBusqueda);
            FormLM.Show();
            this.Hide();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            Menu MenuP = new Menu();
            MenuP.Show();
            this.Hide();

        }

        private void btnRegistrarMascota_Click(object sender, EventArgs e)
        {
            RegistrarMascota FormRMascota = new RegistrarMascota();
            FormRMascota.Show();
            this.Hide();
        }

        private void ListaMascotas_Load(object sender, EventArgs e)
        {

            N_Mascotas ListarMascotas = new N_Mascotas();
            DataTable tablaMascotas = new DataTable();
            tablaMascotas = ListarMascotas.LMascotas(this.cadenaBusqueda);

            dataGridMascotas.DataSource = tablaMascotas;
            dataGridMascotas.Columns[2].HeaderText = "Codigo";
            dataGridMascotas.Columns[3].HeaderText = "Nombre del Propietario";
            dataGridMascotas.Columns[4].HeaderText = "Telefono del Propietario";
            dataGridMascotas.Columns[5].HeaderText = "Mascota";
            dataGridMascotas.Columns[6].HeaderText = "Raza";
            dataGridMascotas.Columns[7].HeaderText = "Edad";
            dataGridMascotas.Columns[8].HeaderText = "Genero";
            dataGridMascotas.Columns[9].HeaderText = "Tipo de Mascota";
            dataGridMascotas.Columns[10].HeaderText = "Registrado por";

        }


        private void dataGridMascotas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridMascotas.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType().Name != "DataGridViewLinkCell") {
                return;
            }
            DataGridViewLinkCell cell = (DataGridViewLinkCell)dataGridMascotas.Rows[e.RowIndex].Cells[e.ColumnIndex];

            if (cell.Value.ToString() == "Editar")
            {
                this.Hide();
                int Id, TelefonoPropietario;
                string NombrePropietario, NombreMascota, RazaMascota, EdadMascota, GeneroMascota, TipoMascota;
                Id = int.Parse((dataGridMascotas.Rows[e.RowIndex].Cells[2]).Value.ToString());
                NombrePropietario = dataGridMascotas.Rows[e.RowIndex].Cells[3].Value.ToString();
                TelefonoPropietario = int.Parse(dataGridMascotas.Rows[e.RowIndex].Cells[4].Value.ToString());
                NombreMascota = dataGridMascotas.Rows[e.RowIndex].Cells[5].Value.ToString();
                RazaMascota = dataGridMascotas.Rows[e.RowIndex].Cells[6].Value.ToString();
                EdadMascota = dataGridMascotas.Rows[e.RowIndex].Cells[7].Value.ToString();
                GeneroMascota = dataGridMascotas.Rows[e.RowIndex].Cells[8].Value.ToString();
                TipoMascota = dataGridMascotas.Rows[e.RowIndex].Cells[9].Value.ToString();
                EditarMascota Ed = new EditarMascota();
                Ed.MostrarInfoMascota(Id, NombrePropietario, TelefonoPropietario, NombreMascota, RazaMascota, EdadMascota, GeneroMascota, TipoMascota);

            }
            else if (cell.Value.ToString() == "Eliminar")
            {
                int Id;
                Id = int.Parse((dataGridMascotas.Rows[e.RowIndex].Cells[2]).Value.ToString());
                N_Mascotas EM = new N_Mascotas();
                EM.EliminarMascota(Id);
                MessageBox.Show("Mascota eliminada exitosamente", "Eliminación", MessageBoxButtons.OK, MessageBoxIcon.Error);

                ListaMascotas FormLM = new ListaMascotas();
                this.Hide();
                FormLM.Show();
            }
        }
    }
}
