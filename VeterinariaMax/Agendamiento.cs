﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace VeterinariaMax
{
    public partial class Agendamiento : Form
    {
        public Agendamiento()
        {
            InitializeComponent();
        }

        private void BtnMenu_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea Salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Agendamiento_Load(object sender, EventArgs e)
        {
            try
            {
                ListaMascotas();
                ListaServicios();
                ConsultaGeneralMascotasServicios();
            }
            catch
            {
                MensajeError();
            }
        }

        private void TxtBusqueda_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ConsultaEspecifica();
            }
            catch
            {
                MensajeError();
            }


        }

        private void TxtBusqueda_Click(object sender, EventArgs e)
        {
            TxtBusqueda.Clear();
        }

        private void BtnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                ValidacionesFormulario();
                
            }
            catch
            {
                MensajeError();
            }
}

        private void ConsultaGeneralMascotasServicios()
        {
            N_Agendamiento agendamiento = new N_Agendamiento();
            DataTable tabla = new DataTable();
            tabla = agendamiento.ConsultaGeneral();
            dataGridConsulta.DataSource = tabla;
            if (dataGridConsulta.Rows.Count == 0)
            {
                LblMensaje.Visible = true;
            }
            else
            {
                dataGridConsulta.DataSource = tabla;
                LblMensaje.Visible = false;
            }
        }

        private void ListaMascotas()
        {
            N_Agendamiento CargaMascotaServicios = new N_Agendamiento();
            DataTable table = new DataTable();
            table = CargaMascotaServicios.ConsultaListaMascotas();

            comboBoxMascotas.DataSource = table;
            comboBoxMascotas.DisplayMember = "Mascota";
            comboBoxMascotas.ValueMember = "IdMascota";

            comboBoxMascotas.Text = "Seleccionar...";
        }

        private void ListaServicios()
        {
            N_Agendamiento CargaMascotaServicios = new N_Agendamiento();
            DataTable table = new DataTable();
            table = CargaMascotaServicios.ConsultaListaServicios();

            comboBoxServicios.DataSource = table;
            comboBoxServicios.DisplayMember = "Servicios";
            comboBoxServicios.ValueMember = "IdServicios";

            comboBoxServicios.Text = "Seleccionar...";
        }

        private void ValidacionesFormulario()
        {
            if(comboBoxMascotas.Text == "Seleccionar..." || comboBoxServicios.Text == "Seleccionar...")
            {
                MessageBox.Show("Seleccione la mascota y el servicio para continuar","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                RegistroFormulario();
            }
        }

        private void MensajeError()
        {
            MessageBox.Show("Error, revisar conexión o procesos con la BD", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ConsultaEspecifica()
        {
            string busqueda = TxtBusqueda.Text;
            N_Agendamiento busquedaDato = new N_Agendamiento();
            DataTable tabla = new DataTable();
            tabla = busquedaDato.ConsultaEspecifica(busqueda);
            dataGridConsulta.DataSource = tabla;
            if (dataGridConsulta.Rows.Count == 0)
            {
                LblMensaje.Visible = true;
            }
            else
            {
                dataGridConsulta.DataSource = tabla;
                LblMensaje.Visible = false;
            }
        }

        private void RegistroFormulario()
        {
            Int32 idMascotas = Convert.ToInt32(comboBoxMascotas.SelectedValue.ToString());
            Int32 idServicios = Convert.ToInt32(comboBoxServicios.SelectedValue.ToString());
            Int32 idUsuarios = 1;
            DateTime fecha = dateTimePickerFecha.Value;
            N_Agendamiento registro = new N_Agendamiento();
            int respuesta = registro.RegistroMascotasServicios(idMascotas, idServicios, idUsuarios, fecha);
            if (respuesta == 0)
            {
                MessageBox.Show("Se registro con exito", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ConsultaGeneralMascotasServicios();
                LimpiarFormulario();
            }
        }

        private void LimpiarFormulario()
        {
            comboBoxMascotas.Text = "Seleccionar...";
            comboBoxServicios.Text = "Seleccionar...";
        }

    }
}
