﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace VeterinariaMax
{


    public partial class RegistrarMascota : Form
    {
       

        public RegistrarMascota()
        {
            InitializeComponent();
        }

        public void btnCrearMascota_Click(object sender, EventArgs e)
        {

            string NombrePropietario = txtNombrePropietario.Text;
            int TelefonoPropietario = int.Parse(txtTelPropietario.Text);
            string NombreMascota = txtNombreMascota.Text;
            string RazaMascota = txtRaza.Text;
            string EdadMascota = txtEdadMascota.Text;
            string GeneroMascota = txtGenero.Text;
            string TipoMascota = txtTipoMascota.Text;


            N_Mascotas RegistroMascota = new N_Mascotas();

            int res = RegistroMascota.RegistroMascota(NombrePropietario, TelefonoPropietario, NombreMascota, RazaMascota, EdadMascota, GeneroMascota, TipoMascota);

            if (res == 1)
            {
                ListaMascotas listMas = new ListaMascotas();
                listMas.Show();
                this.Hide();
                MessageBox.Show("Mascota registrada exitosamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            else
            {
                MessageBox.Show("No se pudo crear la mascota", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }






        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            ListaMascotas listM = new ListaMascotas();
            listM.Show();
            this.Hide();
        }

        private void RegistrarMascota_Load(object sender, EventArgs e)
        {

        }
    }
}
