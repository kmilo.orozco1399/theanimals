﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace VeterinariaMax
{
    public partial class EditarMascota : Form
    {
        public EditarMascota()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void EditarMascota_Load(object sender, EventArgs e)
        {
          


        }



        public void MostrarInfoMascota(int Id, string NombrePropietario, int TelefonoPropietario, string NombreMascota, string RazaMascota, string EdadMascota, string GeneroMascota, string TipoMascota)
        {
            txtIdMascota.Text = Id.ToString();
            txtNombrePropietarioE.Text = NombrePropietario;
            txtTelPropietarioE.Text = TelefonoPropietario.ToString();
            txtNombreMascotaE.Text = NombreMascota;
            txtRazaE.Text = RazaMascota;
            txtEdadMascotaE.Text = EdadMascota;
            txtGeneroE.Text = GeneroMascota;
            txtTipoMascotaE.Text = TipoMascota; 
            this.Show();
        }

        private void btnEditarMascota_Click(object sender, EventArgs e)
        {
            int IdMascota = int.Parse(txtIdMascota.Text);
            string NombrePE = txtNombrePropietarioE.Text;
            int TelefonoPE = int.Parse(txtTelPropietarioE.Text);
            string NombreME = txtNombreMascotaE.Text;
            string RazaME = txtRazaE.Text;
            string EdadME = txtEdadMascotaE.Text;
            string GeneroME = txtGeneroE.Text;
            string TipoME = txtTipoMascotaE.Text;

            N_Mascotas EditarM = new N_Mascotas();
            EditarM.EditarMascota(IdMascota , NombrePE, TelefonoPE, NombreME, RazaME, EdadME, GeneroME, TipoME);

            this.Hide();
            ListaMascotas FormLM = new ListaMascotas();
            FormLM.Show();
            MessageBox.Show("Mascota Editada exitosamente", "Modificación", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //this.Close();
            ListaMascotas listM = new ListaMascotas();
            listM.Show();
            this.Hide();

        }
    }


}

