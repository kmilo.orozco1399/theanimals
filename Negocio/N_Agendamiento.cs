﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class N_Agendamiento
    {
        private int mascota { get; set; }
        private int servicio { get; set; }
        private int usuario { get; set; }
        private DateTime fecha { get; set; }
        private string busquedas { get; set; }

        public DataTable ConsultaGeneral ()
        {
            return D_Agendamiento.Consulta_General();
        }

        public DataTable ConsultaListaMascotas()
        {
            return D_Agendamiento.Consulta_ListaMascotas();
        }

        public DataTable ConsultaListaServicios()
        {
            return D_Agendamiento.Consulta_ListaServicios();
        }

        public int RegistroMascotasServicios(int mascotas, int servicios, int usuarios, DateTime fecha)
        {
            this.mascota = mascotas;
            this.servicio = servicios;
            this.usuario = usuarios;
            this.fecha = fecha;
            return D_Agendamiento.Registro_MascotasServicios(mascota,servicio,usuario,fecha);
        }

        public DataTable ConsultaEspecifica(string busqueda)
        {
            this.busquedas = busqueda;
            return D_Agendamiento.Consulta_Especifica(busqueda);
        }
    }
}
