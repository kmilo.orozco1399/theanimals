﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
   public class N_Login
    {
        private string usuario { get; set; }
        private string contrasena { get; set; }

        public DataTable ValidacionIngreso (string usuario, string contrasena)
        {
            this.usuario = usuario;
            this.contrasena = contrasena;
            return D_Login.ValidacionDBIngreso(usuario,contrasena);
        }
    }
}
