﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class N_Mascotas
    {
        private string NombrePropietario { set; get; }
        private int TelefonoPropietario { set; get; }
        private string NombreMascota { set; get; }
        private string RazaMascota { set; get; }
        private string EdadMascota { set; get; }
        private string GeneroMascota { set; get; }
        private string TipoMascota { set; get; }


        public DataTable LMascotas(string cadenaBusqueda = "")
        {
            return D_Mascota.ListaMascotas(cadenaBusqueda);
        }

        //public int RegistrarMascota(N_Mascotas nuevaMascota) {
        //    nuevaMascota.EliminarMascota(0);
        //    return 0;
        //    //D_Mascota.CrearMascotaDB(nuevaMascota);
        //}

        public int RegistroMascota (string NombrePropietario, int TelefonoPropietario, string NombreMascota, string RazaMascota, string EdadMascota, string GeneroMascota, string TipoMascota) {

            //this.NombrePropietario = NombrePropietario;
            //this.TelefonoPropietario = TelefonoPropietario;
            //this.NombreMascota = NombreMascota;
            //this.RazaMascota = RazaMascota;
            //this.EdadMascota = EdadMascota;
            //this.GeneroMascota = GeneroMascota;
            //this.TipoMascota = TipoMascota;


            return D_Mascota.CrearMascotaDB(NombrePropietario, TelefonoPropietario, NombreMascota, RazaMascota, EdadMascota, GeneroMascota, TipoMascota);
        }


        public int EditarMascota(int IdMascota, string NombPropE, int TelPropE, string NomMascotaE, string RazaMascotaE, string EdadMascotaE, string GeneroMascotaE, string TipoMascotaE)
        {

            //this.NombrePropietario = NombPropE;
            //this.TelefonoPropietario = TelPropE;
            //this.NombreMascota = NomMascotaE;
            //this.RazaMascota = RazaMascotaE;
            //this.EdadMascota = EdadMascotaE;
            //this.GeneroMascota = GeneroMascotaE;
            //this.TipoMascota = TipoMascotaE;


            return D_Mascota.EditarMascotaDB(IdMascota, NombPropE, TelPropE, NomMascotaE, RazaMascotaE, EdadMascotaE, GeneroMascotaE, TipoMascotaE);
        }





        public int EliminarMascota(int Id) {

            int IdMascota = Id;


            return D_Mascota.EliminarMascotaDB(IdMascota);



        }


    }
}
