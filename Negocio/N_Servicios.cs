﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class N_Servicios
    {
        private string Nombre { get; set; }
        private string duracion { get; set; }
        private string Id { get; set; }



        public int RegistrarServicios(string Nombre, string duracion)
        {
            this.Nombre = Nombre;
            this.duracion = duracion;
            return D_Servicios.RegistroDBServicios(Nombre, duracion);
        }
        public int ActualizarServicios(string Nombre, string duracion, string Id)
        {
            this.Nombre = Nombre;
            this.duracion = duracion;
            this.Id = Id;

            return D_Servicios.ActualizarDBServicios(Nombre, duracion, Id);
        }
        public int EliminarServicios(string Id)
        {
            this.Id = Id;

            return D_Servicios.EliminarDBServicios(Id);
        }

        public DataTable ConsultaGeneral()
        {
            return D_Servicios.Consulta_General();
        }
    }
}

